
# json-server --watch db.json

import requests

host = "http://localhost:3000/"

def test(point, method=None):
    url = "{0}{1}".format(host, point)
    if method == "post":
        r = requests.post(url, None)
    elif method == "del":
       r = requests.delete(url)


if __name__ == "__main__":
    #test("games?name=IIIIIPsao&publicKey=12938uadiuiuaidua", "post")

    test("games/2", "del")
