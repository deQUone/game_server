from django.db import models


class GameRoom(models.Model):
    publicKey = models.CharField(max_length=50)
    map_name = models.CharField(max_length=50, default="map1")
    players_online = models.IntegerField(default=0)
    chosenMap = models.IntegerField(default=0)
    blocks = models.CharField(max_length=1024)
    created = models.BigIntegerField(default=0)  # to remove empty games, update it on every new player/data


class Player(models.Model):
    created = models.BigIntegerField(default=0)
    last_fetch = models.BigIntegerField(default=0)  # to get only actual updates

    game = models.ForeignKey(GameRoom)
    playerID = models.CharField(max_length=50)
    nickname = models.CharField(max_length=50)
    skin = models.IntegerField(default=0)

    x = models.FloatField(default=0.0)
    y = models.FloatField(default=0.0)

    score = models.IntegerField(default=0)
    health = models.IntegerField(default=100)
    action = models.CharField(max_length=50)


class Data(models.Model):
    created = models.BigIntegerField(default=0)  # timestamp
    game = models.ForeignKey(GameRoom)
    playerID = models.CharField(max_length=50)   # unique string
    className = models.CharField(max_length=50)  # CPP class name, "player" if we'll have bots
    message = models.CharField(max_length=50)
    # current pos
    x = models.FloatField(default=0.0)
    y = models.FloatField(default=0.0)
    targx = models.FloatField(default=0)
    targy = models.FloatField(default=0)
    # phys property
    velx = models.FloatField(default=0.0)
    vely = models.FloatField(default=0.0)
    rotation = models.FloatField(default=0.0)
    angularVel = models.FloatField(default=0.0)
