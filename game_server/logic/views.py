# encoding: utf8
from django.views.decorators.http import require_GET
from django.http import JsonResponse
from uuid import uuid4
from time import time
import random
import json
from math import sqrt
from logic.models import GameRoom, Player, Data


def throw_error(e):
    return JsonResponse(
        {
            "response":
                [
                    {
                        "type": "error",
                        "message": str(e),
                    }
                ]
        },
        safe=False
    )


# to create terrain json
terrain = (
    ((0, 0), (0, 80), (-1, 60), (-1, 0)),  # -1 means full size on a current device
    ((0, 0), (0, 60), (-1, 80), (-1, 0)),
)


def get_terrain(chosen_map):
    return terrain[chosen_map]


# test it - http://185.22.61.117/get_games/
@require_GET
def get_games(request):
    response = []
    append = response.append
    for game in GameRoom.objects.order_by('-created').all()[:5]:
        append(
            {
                "type": "list",
                "publicKey": game.publicKey,
                "online": str(game.players_online),
                "name": game.map_name,
                "created": game.created,
                "chosenMap": game.chosenMap,
                "blocks": json.loads(game.blocks),
                "terrain": get_terrain(game.chosenMap)
            }
        )
    return JsonResponse(
        {
            "response": response
        },
        safe=False
    )  # safe=False to return lists


# simplify DB management
def create_new_player(game, skin, nickname, x, y):
    return Player.objects.create(
        game=game,
        skin=skin,
        nickname=nickname,
        x=x,
        y=y,
        playerID="0-{0}".format(uuid4())[:50],
        created=int(time()*10000),
        last_fetch=int(time()*10000),
        action="create"
    )


# return a packed Player data
def create_player_pack(item):
    return {
        "type": "create",
        "playerID": item.playerID,
        "skin": item.skin,
        "nickname": item.nickname,
        "x": item.x,
        "y": item.y,
        "score": item.score,
        "health": item.health,
    }


def check_dist(item1, item2):
    if sqrt(item1[0]*item2[0] + item1[1]*item2[1]) > 100:
        return True
    else:
        return False


def generate_blocks():
    res = []
    append = res.append
    num = random.randint(2, 4)
    for i in range(num):
        # structure:             x                          y                     angle
        buf = [random.randint(100, 800), random.randint(100, 800), random.randint(0, 360)]

        # check if no crosses
        flag = 1
        attemp = 0
        while flag != 0:
            flag = 0
            for item in res:
                if not check_dist(buf, item):
                    flag = 1
                    break
            if flag == 1:
                buf = [random.randint(100, 800), random.randint(100, 800), random.randint(0, 360)]
            else:
                append(buf)

            attemp += 1
            if attemp > 10:
                break
    return res

# http://vksmm.info/game_api/create_game/?nickname=Moasdo&x=200&y=200&skin=1&chosenMap=0&gameName=WOWGAME

@require_GET
def create_game(request):
    try:
        nickname = request.GET.get("nickname")
        x = float(request.GET.get("x"))
        y = float(request.GET.get("y"))
        skin = request.GET.get("skin")
        chosenMap = int(float(request.GET.get("chosenMap")))
        map_name = request.GET.get("gameName")

        terrain_blocks = generate_blocks()

        new_game = GameRoom.objects.create(
            publicKey="{0}".format(uuid4())[:50],
            map_name=map_name,
            players_online=1,
            chosenMap=chosenMap,
            blocks=terrain_blocks,
            created=int(time()*10000)
        )
        new_player = create_new_player(
            new_game,
            skin,
            nickname,
            x,
            y
        )
        return JsonResponse(
            {
                "response":
                    [
                        {
                            "type": "new_game",
                            "publicKey": new_game.publicKey,
                            "playerID": new_player.playerID,
                            "terrain": get_terrain(chosenMap),
                            "blocks": terrain_blocks
                        }
                    ]
            },
            safe=False
        )
    except Exception as e:
        return throw_error(e)


@require_GET
def connect(request):
    try:
        skin = request.GET.get("skin")
        nickname = request.GET.get("nickname")
        x = float(request.GET.get("x"))
        y = float(request.GET.get("y"))
        publicKey = request.GET.get("publicKey")

        game = GameRoom.objects.get(publicKey=publicKey)
        game.players_online += 1
        game.created = int(time()*10000)
        game.save()

        opponents = []
        append = opponents.append
        for item in Player.objects.filter(game=game):
            append(create_player_pack(item))

        new_player = create_new_player(
            game,
            skin,
            nickname,
            x,
            y
        )
        append(
            {
                "type": "connect",
                "playerID": new_player.playerID
            }
        )
        return JsonResponse(
            {
                "response": opponents
            },
            safe=False
        )
    except GameRoom.DoesNotExist:
        return throw_error("Game does not exist")


@require_GET
def apply_updates(request):
    try:
        publicKey = request.GET.get("publicKey")
        playerID = request.GET.get("playerID")
        message = request.GET.get("message", "move")
        # current pos
        x = float(request.GET.get("curx"))
        y = float(request.GET.get("cury"))
        # target pos
        targx = float(request.GET.get("targx"))
        targy = float(request.GET.get("targy"))

        # save update (dynamic)
        game = GameRoom.objects.get(publicKey=publicKey)
        game.created = int(time()*10000)
        game.save()

        # save update (static, for new players) + add health, score...
        Player.objects.filter(game=game, playerID=playerID).update(x=x, y=y, action=message)

        Data.objects.create(
            created=int(time()*10000),
            game=game,
            playerID=playerID,
            className="player",
            message=message,
            x=x,
            y=y,
            targx=targx,
            targy=targy
        )
        return JsonResponse({}, safe=False)
    except GameRoom.DoesNotExist:
        return throw_error("Game does not exist")


@require_GET
def fetch_updates(request):
    try:
        publicKey = request.GET.get("publicKey")
        playerID = request.GET.get("playerID")
        # current pos
        curx = float(request.GET.get("curx"))
        cury = float(request.GET.get("cury"))

        game = GameRoom.objects.get(publicKey=publicKey)
        me = Player.objects.get(game=game, playerID=playerID)

        response = []
        append = response.append
        for item in Data.objects.filter(game=game, created__gte=me.last_fetch).exclude(playerID=playerID):
            append({
                "type": item.message,
                "playerID": item.playerID,
                "className": item.className,
                "curx": item.x,
                "cury": item.y,
                "targx": item.targx,
                "targy": item.targy
            })

        # check if any new players
        for item in Player.objects.filter(game=game, created__gte=me.last_fetch).exclude(playerID=playerID):
            append(create_player_pack(item))

        # update my last fetch time and current pos
        me.last_fetch = int(time()*10000)
        me.x = curx  # will work only for new players(((
        me.y = cury
        me.save()
        return JsonResponse(
            {
                "response": response
            },
            safe=False
        )
    except GameRoom.DoesNotExist:
        return throw_error("This game not found")
    except Player.DoesNotExist:
        return throw_error("Player not found")


@require_GET
def exit_game(request):
    try:
        publicKey = request.GET.get("publicKey")
        playerID = request.GET.get("playerID")
        # remove all player data
        game = GameRoom.objects.get(publicKey=publicKey)
        Player.objects.filter(game=game, playerID=playerID).delete()
        Data.objects.filter(game=game, playerID=playerID).delete()
        Data.objects.create(game=game, playerID=playerID, created=int(time()*10000), className="player", message="exit")
        game.players_online -= 1
        if game.players_online == 0:
            game.delete()
        else:
            game.save()
        return JsonResponse({}, safe=False)  # return an empty JSON
    except Exception as e:
        return throw_error(e)
