from django.conf.urls import patterns, url
from logic.views import get_games, create_game, connect, exit_game, apply_updates, fetch_updates

urlpatterns = patterns('',
    url(r'^get_games/$', get_games, name="get_games"),
    url(r'^create_game/$', create_game, name="create_game"),
    url(r'^connect/$', connect, name='connect'),
    url(r'^update/$', apply_updates, name='update'),
    url(r'^fetch_updates/$', fetch_updates, name='fetch_updates'),
    url(r'^exit/$', exit_game, name='exit'),
)
