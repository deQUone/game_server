from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^game_api/', include('logic.urls', namespace='logic')),
)
