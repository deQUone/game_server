# coding:utf8
#
# delete empty games / timeouts
#

# 1) import django

import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "game_server.settings")
django.setup()

# 2) import models, etc else

from time import sleep, time
from logic.models import GameRoom, Player, Data

dt = 300  # 5 min 

if __name__ == "__main__":
    current_time = int(time()*10000)
    all_games = GameRoom.objects.filter(created__lte=current_time-dt*10000)

    for game in all_games:
        Player.objects.filter(game=game).delete()
        Data.objects.filter(game=game).delete()
    all_games.delete()
